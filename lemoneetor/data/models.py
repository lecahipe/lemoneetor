from django.db import models
from django.db.models.base import Model
from data.choices import Side
import uuid

class GenericModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract=True

class Instrument(GenericModel):
    code = models.CharField(max_length=12)
    description = models.CharField(max_length=200)
    name = models.CharField(max_length=128)

class Order(GenericModel):
    isin = models.ForeignKey(Instrument, on_delete=models.PROTECT)
    limit_price = models.FloatField() 
    quantity = models.IntegerField()
    side = models.CharField(max_length=10, choices=Side.choices)
    valid_until = models.DateTimeField()