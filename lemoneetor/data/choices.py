from enum import Enum

class Side:
    BUY = "BUY"
    SELL = "SELL"
    choices = (
        (BUY, "Buy"),
        (SELL, "Sell"),
    )

