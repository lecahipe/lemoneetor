import pytest
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from api.views import OrderView
from data.models import Instrument
from data.choices import Side
from django.utils import timezone
from .factories_orders import InstrumentFactory
import json

url = reverse("api:orders")

def make_post_request(data):
    request = APIRequestFactory().post(url, data=data, format="json")
    view = OrderView.as_view()
    response = view(request)
    response.render()
    return response

def make_get_request():
    request = APIRequestFactory().get(url)
    view = OrderView.as_view()
    response = view(request)
    return response.render()


def test_get_order():
    response = make_get_request()
    assert response.status_code == 405
    assert response.status_text == 'Method Not Allowed'


@pytest.mark.django_db
def test_create_order_sucess():
    data = {
        'isin': InstrumentFactory().pk,
        'limit_price': 1,
        'quantity': 1,
        'side': Side.BUY,
        'valid_until': timezone.now()
    }
    response = make_post_request(data)
    assert response.status_code == 201


@pytest.mark.django_db
def test_create_order_quantity_error():
    data = {
        'isin': InstrumentFactory().pk,
        'limit_price': 1,
        'quantity': -1,
        'side': Side.BUY,
        'valid_until': timezone.now()
    }
    response = make_post_request(data)
    content = json.loads(response.content)
    assert "Quantity should be higher than 0." in content.get('quantity')
    assert response.status_code == 400


@pytest.mark.django_db
def test_create_order_limit_error():
    data = {
        'isin': InstrumentFactory().pk,
        'limit_price': -1,
        'quantity': 2,
        'side': Side.BUY,
        'valid_until': timezone.now()
    }
    response = make_post_request(data)
    content = json.loads(response.content)
    assert "Limit Price should be higher than 0." in content.get('limit_price')
    assert response.status_code == 400


