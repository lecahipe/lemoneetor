import factory
from data.models import Instrument

from factory import fuzzy


class InstrumentFactory(factory.django.DjangoModelFactory):
    name = fuzzy.FuzzyText(length=128)
    description = fuzzy.FuzzyText(length=200)
    code = fuzzy.FuzzyText(length=12)
    
    class Meta:
        model = Instrument