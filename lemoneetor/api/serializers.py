from data.models import Order
from rest_framework import serializers


class OrderCreateSerializer(serializers.ModelSerializer):

    def validate(self, data):
        quantity = data.get('quantity')
        limit_price = data.get('limit_price')
        
        if not quantity > 0:
            raise serializers.ValidationError({'quantity':'Quantity should be higher than 0.'})
        
        if not limit_price > 0:
            raise serializers.ValidationError({'limit_price':'Limit Price should be higher than 0.'})
        
        return data
    class Meta:
        model = Order
        fields = ['isin', 'limit_price', 'quantity', 'side', 'valid_until']