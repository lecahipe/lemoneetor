from data.models import Order
from api.serializers import OrderCreateSerializer

from rest_framework import generics
class OrderView(generics.CreateAPIView):
    serializer_class = OrderCreateSerializer
    queryset = Order.objects.all()
