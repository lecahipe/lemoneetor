from django.urls import path
from api.views import OrderView

app_name="api"

urlpatterns = [
    path('orders/', OrderView.as_view(), name="orders"),
]
